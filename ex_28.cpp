// Экзаменационное задание №28
// Написать функцию для транспонирования матрицы
#include <iostream>
#include <ctime>
using namespace std;
// алгоритм транспонирования матрицы
void Transpose(int **arr, int size) {
    int tmp;
    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            tmp = arr[i][j];
            arr[i][j] = arr[j][i];
            arr[j][i] = tmp;
        }
    }
}

int main() {
    srand(time(NULL));
    int size = 3;
    // создаем динамический массив
    int **matrix;
    matrix = new int*[size];
    for (int i = 0; i < size; i++) {
        matrix[i] = new int[size];
    }
    // заполняем массив случайными числами
    cout << "Matrix: " << endl;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            // генерирует случайные числа от 0 до 9
            matrix[i][j] = rand() % 10;
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    // выводим транспонированный массиы
    cout << "Transpose:" << endl;
    Transpose(matrix, size);
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    // очищаем массив
    for(int i = 0; i < size; i++) {
        delete[]matrix[i];
    }
    return 0;
}
