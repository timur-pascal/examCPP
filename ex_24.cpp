// Экзаменационное задание №24
// Найти все простые числа из диапозона от N до M,
// где N < M < 256
#include <iostream>
using namespace std;
// определение простого числа
bool isPrime(int n) {
    // перебор делителей
    for (int i = 2; i * i <= n; i++) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

int main() {
    int N;
    int M;
    // ввод отрезка
    cout << "Input N and M: ";
    cin  >> N >> M;
    // проверка ввода
    if (N < M && M < 256) {
        // итерация по отрезку
        for (int i = N; i < M; i++){
            if (isPrime(i)) {
                cout << i << endl;
            }
        }
    }
    else {
        cout << "Error input!" << endl;
    }
    return 0;
}
