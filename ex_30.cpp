// Экзаменационное задание №30
// написать функцию для сортировки строк матрицы в обратном порядке
#include <iostream>
#include <ctime>
using namespace std;
// сортирует строки матрицы в обратном порядке
void reverse(int **arr, int col, int line) {
    int tmp;
    for (int i = 0; i < col; i++) {
        for (int j = 0; j < line / 2; j++) {
            tmp = arr[i][j];
            arr[i][j] = arr[i][line - j - 1];
            arr[i][line - j - 1] = tmp;
        }
    }
}

int main() {
    srand(time(NULL));
    const int COL = 3, LINE = 5;
    // создаем динамический массив
    int **matrix;
    matrix = new int*[COL];
    for (int i = 0; i < COL; i++) {
        matrix[i] = new int[LINE];
    }
    // заполняем массив случайными числами
    cout << "Matrix: " << endl;
    for (int i = 0; i < COL; i++) {
        for (int j = 0; j < LINE; j++) {
            // генерирует случайные числа от 0 до 9
            matrix[i][j] = rand() % 10;
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    // вывод отсортированной матрицы
    cout << "Reverse:" << endl;;
    reverse(matrix, COL, LINE);
    for (int i = 0; i < COL; i++) {
        for (int j = 0; j < LINE; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    // очистка массива
    for(int i = 0; i < COL; i++) {
        delete[]matrix[i];
    }
    return 0;
}
